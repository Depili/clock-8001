module gitlab.com/clock-8001/clock-8001/v4

go 1.17

require (
	github.com/chabad360/go-osc v0.0.0-20220217020417-1229c4fc60a5
	github.com/denisbrodbeck/machineid v1.0.1
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f
	github.com/husl-colors/husl-go v2.0.0+incompatible // indirect
	github.com/jessevdk/go-flags v1.4.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	github.com/veandco/go-sdl2 v0.4.27
	gitlab.com/Depili/go-osc v0.0.0-20220126123138-cdbd92c9027a
	gitlab.com/Depili/go-rgb-led-matrix v0.0.0-20170204170814-25b63d4baf68
	gitlab.com/Depili/limitimer v0.0.0-20220312082356-1cec6f97c270
	gitlab.com/Depili/ubercorn v1.0.0
	golang.org/x/sys v0.0.0-20211205182925-97ca703d548d // indirect
	golang.org/x/text v0.3.7
	periph.io/x/periph v3.6.7+incompatible
)

require github.com/icholy/digest v0.1.15

require (
	github.com/sigurn/crc16 v0.0.0-20211026045750-20ab5afb07e3 // indirect
	github.com/stianeikeland/go-rpio/v4 v4.6.0 // indirect
	gitlab.com/clock-8001/futaba_vfd v0.0.0-20240531201016-091b45c8cf5e // indirect
	periph.io/x/conn/v3 v3.6.7 // indirect
	periph.io/x/host/v3 v3.6.7 // indirect
)
